psi-lang: psi-lang (Language pack for Psi)
psi-lang:
psi-lang: Psi is a multi-platform Jabber client.
psi-lang: This package adds several translations as a package to the Psi
psi-lang: installation.
psi-lang: Available are:
psi-lang: be cs de eo es es_ES fr it ja mk pl pt_BR ru sl sv uk ur_PK vi zh_CN zh_TW
psi-lang:
psi-lang:
psi-lang: Homepage is http://psi-im.org/
psi-lang:
