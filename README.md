creating a new clean branch:
```
$ rm -rf .git/ ; git init && git remote add origin git@gitlab.com:username/projectpath.git
$ git add --all -v && git commit -a -m 'update' -v && git push -f origin master
```

after adding new folder:
```
$ git add --all -v && git commit -a -m 'update' -v && git push origin master
```

if everything was deleted:
```
$ git clone git@gitlab.com:username/projectpath.git
```

size:
```
$ du -sh --exclude=.git
7,1G	.

$ du -h --max-depth=1 | sort -k 2
14G	.
6,9G	./.git
236M	./2010_mopslinux
2,2G	./2013_agilialinux
9,2M	./2014_ubuntu
316K	./2015_salix
104M	./2021_slarm64
4,7G	./2022_slackware
```