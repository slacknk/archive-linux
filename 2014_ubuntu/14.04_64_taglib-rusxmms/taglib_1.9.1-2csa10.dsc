Format: 3.0 (quilt)
Source: taglib
Binary: libtag1c2a, libtag1-vanilla, libtag1-dev, libtag1-doc, libtagc0, libtagc0-dev
Architecture: any all
Version: 1.9.1-2csa10
Maintainer: Modestas Vainius <modax@debian.org>
Homepage: http://taglib.github.io/
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=users/modax/taglib.git;a=summary
Vcs-Git: git://anonscm.debian.org/users/modax/taglib.git
Build-Depends: cmake (>= 2.6.2), debhelper (>= 9), dpkg-dev (>= 1.16.1), zlib1g-dev, pkg-kde-tools (>= 0.14)
Build-Depends-Indep: doxygen, graphviz, gsfonts-x11
Package-List: 
 libtag1-dev deb libdevel optional
 libtag1-doc deb doc optional
 libtag1-vanilla deb libs optional
 libtag1c2a deb libs optional
 libtagc0 deb libs optional
 libtagc0-dev deb libdevel optional
Checksums-Sha1: 
 4fa426c453297e62c1d1eff64a46e76ed8bebb45 654074 taglib_1.9.1.orig.tar.gz
 e8d7f6be28df461bb785a9b30e7c8ad491a39be5 25565 taglib_1.9.1-2csa10.debian.tar.gz
Checksums-Sha256: 
 72d371cd1419a87ae200447a53bff2be219283071e80fd12337928cc967dc71a 654074 taglib_1.9.1.orig.tar.gz
 59ab4863a3f8572522bef96b287d81d6a87644e83151bc38fb3b52102ccb3e2d 25565 taglib_1.9.1-2csa10.debian.tar.gz
Files: 
 0d35df96822bbd564c5504cb3c2e4d86 654074 taglib_1.9.1.orig.tar.gz
 5dda2cbe93852c161b1a0eb2f0b13d37 25565 taglib_1.9.1-2csa10.debian.tar.gz
