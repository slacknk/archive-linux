# taglib-rusxmms - 1.9.1-2 - x86_64

**russians mp3-tags in Clementine, AmaroK, Qmmp**

## ubuntu-14.04 / mint-17

CREATE directory for deb-packages:

    $ mkdir -p /tmp/ubuntu-14.04-64_taglib-rusxmms
    $ cd /tmp/ubuntu-14.04-64_taglib-rusxmms


DOWNLOAD debs: 

    $ wget -c https://git.../slacknk/.../libtag1c2a_1.9.1-2csa10_amd64.deb
    $ wget -c https://git.../slacknk/.../libtag1-vanilla_1.9.1-2csa10_amd64.deb

INSTALL: 

    $ sudo dpkg -i libtag1c2a_1.9.1-2csa10_amd64.deb libtag1-vanilla_1.9.1-2csa10_amd64.deb

DONE! Restart your favorite player and add songs in playlist.

**How build this packages:**

    $ sudo apt-get build-dep taglib
    $ mkdir /tmp/taglib-rusxmms/
    $ cd /tmp/taglib-rusxmms/
    $ wget -c http://darksoft.org/files/rusxmms/patches/taglib-csa10.tar.bz2
    $ tar -xvf taglib-csa10.tar.bz2 -p -C ./
    $ cd /tmp/taglib-rusxmms/
    $ apt-get source taglib
    $ echo taglib-1.9.1-ds-rusxmms.patch >> /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/series
    $ cp -av taglib/taglib-1.9.1-ds-rusxmms.patch /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/
    $ echo taglib-1.9.1-ds-rusxmms-enforce.patch >> /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/series
    $ cp -av taglib/taglib-1.9.1-ds-rusxmms-enforce.patch /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/
    $ rm -v /tmp/taglib-rusxmms/taglib-1.9.1/debian/libtag1-vanilla.symbol
    $ cd /tmp/taglib-rusxmms/taglib-1.9.1
    $ dch -i
    $ dpkg-buildpackage -rfakeroot
    $ sudo dpkg -i /tmp/taglib-rusxmms/libtag1c2a_*.deb /tmp/taglib-rusxmms/libtag1-vanilla_*.deb


***
#### For Russian speakers users
В данном каталоге представлены пакеты taglib_1.9.1-2 для ubuntu-14.04 с наложенными патчами из проекта [RusXMMS](http://rusxmms.sourceforge.net/). Идея пересобрать taglib возникла после следующего: [taglib_1.9.1-2.1_changelog](https://metadata.ftp-master.debian.org/changelogs/main/t/taglib/taglib_1.9.1-2.1_changelog)
```
taglib (1.9.1-1) unstable; urgency=low
  * Drop RusXMMS flavour (libtag1-rusxmms package). The patch no longer
    applies, it is hardly maintained upstream, its popcon is very low etc.
```
Пакеты были собраны в LinuxMint-17 базирующейся на пакетной базе Ubuntu-14.04-LTS.

##### Как собирались или как собрать/пересобрать самому.

Подготовим каталог для сборки:
```
$ sudo apt-get build-dep taglib
$ mkdir /tmp/taglib-rusxmms/
$ cd /tmp/taglib-rusxmms/
$ wget -c http://darksoft.org/files/rusxmms/patches/taglib-csa10.tar.bz2
$ tar -xvf taglib-csa10.tar.bz2 -p -C ./
```
скачаем исходники по которым в ubuntu/deb собирался taglib (необходимо что бы src-репозиторий был подключен) и пропишем патчи, которые мы загрузили и распаковали:
```
$ cd /tmp/taglib-rusxmms/
$ apt-get source taglib

$ echo taglib-1.9.1-ds-rusxmms.patch >> /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/series
$ cp -av taglib/taglib-1.9.1-ds-rusxmms.patch /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/ 

$ echo taglib-1.9.1-ds-rusxmms-enforce.patch >> /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/series 
$ cp -av taglib/taglib-1.9.1-ds-rusxmms-enforce.patch /tmp/taglib-rusxmms/taglib-1.9.1/debian/patches/
```
Укажем различные внесенные изменения:
```
$ cd /tmp/taglib-rusxmms/taglib-1.9.1
$ dch -i
```
* я, например, здесь подправил билд на 2csa10, получилось что-то вида: taglib (1.9.1-2csa10) и в * (изменениях) указал:
  * `* taglib-csa10 < http://rusxmms.sourceforge.net >`

Далее можно приступать к сборке и сборка у меня проходила, но в конце обрывалась на
```
dpkg-buildpackage -rfakeroot
...
dpkg-gensymbols: предупреждение: появилось несколько новых символов в файле symbols: смотрите вывод diff ниже
dpkg-gensymbols: предупреждение: некоторые символы или шаблоны исчезли из файла symbols: смотрите вывод diff ниже
dpkg-gensymbols: предупреждение: debian/libtag1-vanilla/DEBIAN/symbols совпадает с debian/libtag1-vanilla.symbols не полностью

...

dh_makeshlibs: dpkg-gensymbols -plibtag1-vanilla -Idebian/libtag1-vanilla.symbols -Pdebian/libtag1-vanilla -edebian/libtag1-vanilla/usr/lib/x86_64-linux-gnu/libtag.so.1.14.0
 returned exit code 1
make: *** [binary] Ошибка 1
dpkg-buildpackage: ошибка: fakeroot debian/rules binary возвратил код ошибки 2
```
Что я сделал, просто взял и удалил debian/libtag1-vanilla.symbol с которым шел конфликт
```
rm -v /tmp/taglib-rusxmms/taglib-1.9.1/debian/libtag1-vanilla.symbol
```
* перед удалением конечно же заново перезагрузил дерево сборки (apt-get source taglib) и прописал патчи (действия выше) и сборка прошла!
```
dpkg-buildpackage -rfakeroot
```
Пакеты получил и установил конкретные:
```
$ sudo dpkg -i /tmp/taglib-rusxmms/libtag1c2a_1.9.1-2csa10_amd64.deb
$ sudo dpkg -i /tmp/taglib-rusxmms/libtag1-vanilla_1.9.1-2csa10_amd64.deb
```
И теперь у меня в clementine проблем с кодировкой нет и не надо перегонять теги в utf. Да, на всякий случай, все что у меня относящееся к моим действиям 1.9.1-2csa10 и получившееся я сохранил в каталог, а именно:
```
libtag1c2a_1.9.1-2csa10_amd64.deb
libtag1-dev_1.9.1-2csa10_amd64.deb
libtag1-doc_1.9.1-2csa10_all.deb
libtag1-vanilla_1.9.1-2csa10_amd64.deb
libtagc0_1.9.1-2csa10_amd64.deb
libtagc0-dev_1.9.1-2csa10_amd64.deb
taglib_1.9.1-2csa10_amd64.changes
taglib_1.9.1-2csa10.debian.tar.gz
taglib_1.9.1-2csa10.dsc
```
